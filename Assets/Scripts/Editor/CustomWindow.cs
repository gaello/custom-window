﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Custom window class that is displaying sample editor window.
/// </summary>
public class CustomWindow : EditorWindow
{
    // Sample variables that are used to display editing in the window.
    private bool checkbox;
    private float number;
    private string textSample;

    // Click count on the button.
    private int clickCount;

    // Add menu named "Custom Window" to the menu.
    [MenuItem("Window/Custom Window")]
    /// <summary>
    /// Method called to show window.
    /// </summary>
    public static void ShowWindow()
    {
        // Get existing open window or if none, make a new one:
        var window = GetWindow(typeof(CustomWindow));
        window.Show();
    }

    /// <summary>
    /// Unity method that renders editor window.
    /// </summary>
    private void OnGUI()
    {
        EditorGUILayout.LabelField("Simple Custom Window", EditorStyles.boldLabel);
        EditorGUILayout.Space();

        // Sample variables displayed in the window.
        checkbox = EditorGUILayout.Toggle("Sample checkbox", checkbox);
        number = EditorGUILayout.Slider("Sample slider", number, 0, 10);
        textSample = EditorGUILayout.TextField("Sample text", textSample);

        EditorGUILayout.Space();

        // Displaying button with click count.
        if (GUILayout.Button(string.Format("You've clicked me {0} times!", clickCount)))
        {
            clickCount++;
        }
    }
}
